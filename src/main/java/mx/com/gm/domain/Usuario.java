package mx.com.gm.domain;

import java.io.Serializable;
import java.util.List;
import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import lombok.Data;

@Entity
@Data
@Table(name="usuario")
public class Usuario implements Serializable{
    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idUsuario;
    
     //indicamos que el atributo de username no debe de ser vacio
    @NotEmpty
    private String username;
    
    //indicamos que el atributo de password no debe de ser vacio
    @NotEmpty
    private String password;
    
    //indica la relacion que se hara conlas tablas de la base de datos
    @OneToMany
    @JoinColumn(name="id_usuario")
    private List<Rol> roles;
}
